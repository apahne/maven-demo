# README #

This is simply nested maven project used for demonstration purposes and Jenkins configuration checks.

This repository is not necessary for the VGOA system.

Features:

* "full builds" enabled
* dependency management (versions) all in one place: proper use of dependencyManagement feature
* plugin management (versions) all in one place: proper use of pluginManagement feature
* findbugs integration

